#!/bin/sh
SMIG="smig" # replace smig here with a full path, or in future with debug smig dsmig
OUTPUTDIR="$HOME/Desktop/junk/" # output from scripts goes here.
mkdir -p $OUTPUTDIR # create output directory if it doesn't already exist

FILEPATH="$PWD/$1"

IIR=`$SMIG create -type imageimporter -file "$FILEPATH"`

PIXELWIDTH=`$SMIG getproperty -object $IIR -imageindex 0 -property dictionary.PixelWidth`

PIXELHEIGHT=`$SMIG getproperty -object $IIR -imageindex 0 -property dictionary.PixelHeight`

BMC=`$SMIG create -type bitmapcontext -width $PIXELWIDTH -height $PIXELHEIGHT -preset AlphaPreMulFirstRGB8bpcInt`

$SMIG doaction -showwindow YES -object $BMC

$SMIG doaction -drawelement -object $BMC -jsonstring '{ "elementtype" : "drawimage", "objectreference" : '$IIR', "destinationrectangle" : { "origin" : { "x" : 0.0, "y" : 0.0 }, "size" : { "width" : '"$PIXELWIDTH"', "height" : '"$PIXELHEIGHT"' } } }'

TEXTTODRAW=$(basename "$FILEPATH")

$SMIG doaction -drawelement -object $BMC -jsonstring '{ "elementtype" : "drawbasicstring", "stringtext" : "'"$TEXTTODRAW"'", "point" : { "x" : 0.0, "y" : 0.0 }, "postscriptfontname" : "Arial-BoldMT", "fontsize" : 20.0, "fillcolor" : { "gray" : 1.0, "colorcolorprofilename" : "kCGColorSpaceGenericGray" }, "stringstrokewidth" : -4.0, "strokecolor" : { "colorcolorprofilename" : "kCGColorSpaceGenericGray", "gray" : 0.5 }, "arrayofpathelements" : [ { "elementtype" : "pathrectangle", "rect" : { "origin" : { "x" : 0.0, "y" : 0.0 }, "size" : { "height" : 24.0, "width" : '$PIXELWIDTH' } } } ], "textalignment" : "kCTTextAlignmentCenter" }'

IEX=`$SMIG create -type imageexporter -file "~/Desktop/junk/imagewithtext.png" -utifiletype public.png`

$SMIG doaction -addimage -object $IEX -secondaryobject $BMC
$SMIG doaction -export -object $IEX
$SMIG doaction -close -object $IEX
$SMIG doaction -close -object $BMC
$SMIG doaction -close -object $IIR
