#!/usr/bin/env ruby

# Create an image transition sequence that goes from a source to a target image

require 'moving_images'

include MovingImages

# The transition filter to apply:
transition_filter = :CIRippleTransition

# The number of frames to transition from source to destination image
number_of_frames = 30

# The folder where the images will be saved.
# output_dir = nil
output_dir = "~/Desktop/transition"

# You can specify the source image here or let the script ask you to select it.
source_file = nil

if source_file.nil?
  source_file = MILibrary::Utility.request_a_file(message: "Select source file")
  if source_file.eql? "Cancel"
    exit(0)
  end
end

# You can specify the target image here or let script ask you to select it.
target_file = nil

if target_file.nil?
  target_file = MILibrary::Utility.request_a_file(
                                              message: "Select target file")
  if target_file.eql? "Cancel"
    exit(0)
  end
end

if output_dir.nil?
  output_dir = MILibrary::Utility.select_a_folder(
                                        message: "Select Destination Folder")
  if output_dir.eql? "Cancel"
    exit(0)
  end
end

image_dimensions = SpotlightCommand.get_imagedimensions(source_file)
extent = "[0 0 #{image_dimensions[:width]} #{image_dimensions[:height]}]"
center = "[#{image_dimensions[:width] / 2.0} #{image_dimensions[:height]/2.0}]"
puts extent
puts center

the_options = { generate_json: true,
                    outputdir: output_dir,
                  sourceimage: source_file,
                  targetimage: target_file,
               exportfiletype: :'public.tiff',
             transitionfilter: :CIRippleTransition,
                     basename: 'image',
                        count: number_of_frames,
                   inputScale: 40,
                   inputWidth: image_dimensions[:width] * 0.02,
                  inputExtent: extent,
                  inputCenter: center,
                      verbose: false,
                generate_json: false,
               softwarerender: false }

start_time = Time.now
MILibrary.dotransition(the_options)
puts "Time to process: #{Time.now - start_time}"
`open #{the_options[:outputdir]}`
