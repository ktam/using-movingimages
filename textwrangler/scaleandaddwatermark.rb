#!/usr/bin/env ruby

# Scale and add a text water mark to images.

require 'moving_images'

include MovingImages

# The folder where the water marked images will be saved.
output_dir = "~/Desktop/watermarkedfiles"
# output_dir = nil

# You can specify the source directory path here or let script ask you.
# source_directory = File.expand_path("~/Pictures/aatemp")
source_directory = nil

if source_directory.nil?
  source_directory = MILibrary::Utility.select_a_folder
  if source_directory.eql? "Cancel"
    exit(0)
  end
end

if output_dir.nil?
  output_dir = MILibrary::Utility.select_a_folder(
                                          message: "Select Destination Folder")
  if output_dir.eql? "Cancel"
    exit(0)
  end
end

# images = SpotlightCommand.find_imagefiles(onlyin: source_directory)
# image_lists = MILibrary::Utility.make_imagefilelists_forprocessing(
#                                       imagefilelist: images,
#                  assume_images_have_same_dimensions: false)

images = SpotlightCommand.find_imagefiles_withdimensions(
                              width: 2272,
                             height: 1704,
                             onlyin: source_directory)
image_lists = MILibrary::Utility.splitlists_intolists_withmaxnum(images, 50)

almost_black_semi_trans = MIColor.make_rgbacolor(0.1, 0.1, 0.1, a: 0.25)
almost_white_semi_trans = MIColor.make_rgbacolor(0.9, 0.9, 0.9, a: 0.25)

options = MILibrary::Utility.make_addtextwatermark_options(
                              text: "©Kevin Meaney",
                         fillcolor: almost_black_semi_trans,
                       strokecolor: almost_white_semi_trans,
                       strokewidth: -6.0, # 0 means don't stroke.
                          fontsize: nil, # nil means calculate font size.
                              font: 'AvenirNext-Heavy',
                             scale: 0.35,
                         outputdir: output_dir,
                    exportfiletype: :'public.jpeg',
                           quality: 0.8, # jpeg quality setting
                      copymetadata: false,
assume_images_have_same_dimensions: true,
                             async: true,
                           verbose: true)

start_time = Time.now
list_num = 0
num_lists = image_lists.length
lastobject_id = image_lists.last.object_id
image_lists.each do |image_list|
  puts "Number of files in list: #{image_list[:files].size}"
  if lastobject_id.eql? image_list.object_id
    options[:async] = false
  end
  MILibrary.addtextwatermark_files(options, image_list)
  puts "Processed list #{list_num + 1} of #{num_lists}"
  list_num = list_num.succ
end

puts "Time to process: #{Time.now - start_time}"
`open #{options[:outputdir]}`
