#!/usr/bin/env ruby

# Scale images so that width is a specific size

require 'moving_images'

include MovingImages

# The width that all the images should be scaled to:
scale_to = 768

# The folder where the scaled images will be saved.
# output_dir = nil
output_dir = "~/Desktop/scaleto"

# You can specify the source directory path here or let script ask you.
source_directory = nil
# source_directory = File.expand_path("~/Pictures")

if source_directory.nil?
  source_directory = MILibrary::Utility.select_a_folder
  if source_directory.eql? "Cancel"
    exit(0)
  end
end

if output_dir.nil?
  output_dir = MILibrary::Utility.select_a_folder(
                                        message: "Select Destination Folder")
  if output_dir.eql? "Cancel"
    exit(0)
  end
end

images = SpotlightCommand.find_imagefiles(onlyin: source_directory)

options = MILibrary::Utility.make_scaleimages_options(
                                    scalex: nil,
                                    scaley: nil,
                                    outputdir: output_dir,
                                    exportfiletype: :'public.jpeg',
                                    quality: 0.8,
                                    interpqual: :default,
                                    copymetadata: false,
                                    assume_images_have_same_dimensions: false,
                                    async: true,
                                    verbose: true)

puts "Number of files to process: #{images.size}"
have_samedims = options[:assume_images_have_same_dimensions]

process_imageslists = MILibrary::Utility.make_imagefilelists_forprocessing(
                                        imagefilelist: images,
                   assume_images_have_same_dimensions: have_samedims)

start_time = Time.now
list_num = 0
num_lists = process_imageslists.length
lastobject_id = process_imageslists.last.object_id
cum_processed = 0
process_imageslists.each do |image_list|
  puts "======================================="
  num_files_in_list = image_list[:files].size
  cum_processed += num_files_in_list
  puts "Number of files in list: #{num_files_in_list}"
  puts "Original width: #{image_list[:width]}"
  puts "First file in list: #{image_list[:files].first}"
  if lastobject_id.eql? image_list.object_id
    options[:async] = false
  end
  scale = scale_to.to_f / image_list[:width].to_f
  options[:scalex] = scale
  options[:scaley] = scale
  MILibrary.scale_files(options, image_list)
  puts "Processed list #{list_num + 1} of #{num_lists}"
  list_num = list_num.succ
end

puts "======================================="
puts "Number of processed images: #{cum_processed}"
puts "Time to process: #{Time.now - start_time}"
`open #{options[:outputdir]}`
