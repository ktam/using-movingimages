require 'moving_images'
include MovingImages
include CommandModule

# Create bitmap context, just so we can get the property width from it
create_bitmapcontext_command = CommandModule.make_createbitmapcontext(width: 900, height: 600, name: :documentationcontext)
object_reference = Smig.perform_command(create_bitmapcontext_command).to_i
bitmap_object = SmigIDHash.make_objectid(objectreference: object_reference)

# make the get width property command:
get_widthproperty_command = CommandModule.make_get_objectproperty(bitmap_object, property: :width)

# generate the json from the command object hash.
puts JSON.pretty_generate(get_widthproperty_command.commandhash)

# Just for completeness actually request the width.
bitmap_width = Smig.perform_command(get_widthproperty_command).to_i

#
# Get the height of an image:
#
importer_object = SmigIDHash.make_objectid(objecttype: :imageimporter,
                                           objectname: :documentationimporter)
get_imageheight = CommandModule.make_get_objectproperty(importer_object, property: :"dictionary.PixelHeight", imageindex: 0)
puts JSON.pretty_generate(get_imageheight.commandhash)

#
# Copy metadata about an image from an image importer object to exporter image.
#
exporter_object = SmigIDHash.make_objectid(objecttype: :imageexporter, objectname: :documenationexporter)
importer_object = SmigIDHash.make_objectid(objecttype: :imageimporter, objectname: :documentationimporter)
copy_metadata_command = CommandModule.make_copymetadata(exporter_object, importersource: importer_object, imageindex: 0)
puts JSON.pretty_generate(copy_metadata_command.commandhash)

#
# Add an image to an image exporter from image importer & copy metadata command
#
exporter_object = SmigIDHash.make_objectid(objecttype: :imageexporter,
                                           objectname: 'exporter.object')
importer_object = SmigIDHash.make_objectid(objecttype: :imageimporter,
                                           objectname: 'importer.object')
addimage_command = CommandModule.make_addimage(exporter_object,
                                               importer_object,
                                               imageindex: 1,
                                               grabmetadata: true)
puts JSON.pretty_generate(addimage_command.commandhash)

#
# Add an image to an image exporter object from a context with objectreference 0
#
context_object = SmigIDHash.make_objectid(objectreference: 0)
addimage_command = CommandModule.make_addimage(exporter_object, 
                                               context_object)
puts JSON.pretty_generate(addimage_command.commandhash)

#
# Make a close command
#
object = SmigIDHash.make_objectid(objectreference: 0)
close_command = CommandModule.make_close(object)
puts JSON.pretty_generate(close_command.commandhash)

#
# Make a draw element command
#
drawrectangle = MIDrawElement.new(:fillrectangle)
drawrectangle.fillcolor = MIColor.make_rgbacolor(0.6, 0.3, 0.2)
drawrectangle.rectangle = MIShapes.make_rectangle(xloc: 100, yloc: 200, width: 250.0, height: 143.25)
bitmap_object = SmigIDHash.make_objectid(objecttype: :bitmapcontext, objectname: :documentationbitmap)
drawelement_command = CommandModule.make_drawelement(bitmap_object, drawinstructions: drawrectangle)
puts JSON.pretty_generate(drawelement_command.commandhash)

#
# Make an export command, handled by an exporter type object only.
#
exporter_object = SmigIDHash.make_objectid(objectreference: 4)
export_command = CommandModule.make_export(exporter_object)
puts JSON.pretty_generate(export_command.commandhash)

#
# Make a more complex getproperties command.
#
object = SmigIDHash.make_objectid(objecttype: :imageimporter,
                                  objectname: 'test.importer.object')
jsonfile = '~/imageproperties.json'
getproperties_command = CommandModule.make_get_objectproperties(
                                                 object,
                                                 imageindex: 0,
                                                 saveresultstype: :jsonfile,
                                                 saveresultsto: jsonfile)
puts JSON.pretty_generate(getproperties_command.commandhash)

#
# Make a simpler getproperties command.
#
pdf_object = SmigIDHash.make_objectid(objecttype: :pdfcontext,
                                      objectname: 'test.pdfcontext.object')
getproperties_command = CommandModule.make_get_objectproperties(pdf_object,
                                      saveresultstype: :jsonstring)
puts JSON.pretty_generate(getproperties_command.commandhash)

#
# Make a get pixel data command for one pixel
#
bitmap_object = SmigIDHash.make_objectid(objecttype: :bitmapcontext, objectname: :documentationbitmap)
source_rect = MIShapes.make_rectangle(xloc: 0, yloc: 0, width: 1, height: 1)
get_pixeldata = CommandModule.make_getpixeldata(bitmap_object, rectangle: source_rect, resultstype: :jsonstring)
puts JSON.pretty_generate(get_pixeldata.commandhash)

# Here is the version that also creates a bitmap context and returns the
# pixel data for one pixel.
include CommandModule
source_rect = MIShapes.make_rectangle(xloc: 0, yloc: 0, width: 1, height: 1)
the_commands = SmigCommands.new
bitmap_object = the_commands.make_createbitmapcontext(name: :documentationbitmap)
get_pixeldata = CommandModule.make_getpixeldata(bitmap_object, rectangle: source_rect, resultstype: :jsonstring)
the_commands.add_command(get_pixeldata)
pixeldata_hash = JSON.parse(Smig.perform_commands(the_commands))
puts JSON.pretty_generate(pixeldata_hash)

#
# The get_pixeldata version that would save the result to a json file
#
bitmap_object = SmigIDHash.make_objectid(objecttype: :bitmapcontext, objectname: :documentationbitmap)
source_rect = MIShapes.make_rectangle(xloc: 0, yloc: 0, width: 20, height: 1)
get_pixeldata = CommandModule.make_getpixeldata(bitmap_object, rectangle: source_rect, savelocation: '~/examplepixeldata.json')
puts JSON.pretty_generate(get_pixeldata.commandhash)

#
# Set the export compression quality to a image exporter object
#
exporter_object = SmigIDHash.make_objectid(objecttype: :imageexporter, objectname: :documentationexporter)
setproperty_command = CommandModule.make_set_objectproperty(exporter_object, propertykey: :exportercompressionquality, propertyvalue: 0.9, imageindex: 0)
puts JSON.pretty_generate(setproperty_command.commandhash)

