require 'moving_images'
include MovingImages
include CommandModule

commands = SmigCommands.new
commands.stoponfailure = true
commands.informationreturned = :lastcommandresult
commands.run_asynchronously = false
commands.saveresultstype = :jsonfile
commands.stoponfailure = false
bitmap_size = MIShapes.make_size(400, 300)
commands.make_createbitmapcontext(size: bitmap_size)
commands.saveresultsto = "~/commandsresults.json"
puts JSON.pretty_generate(commands.commandshash)
