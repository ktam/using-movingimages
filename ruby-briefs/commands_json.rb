require 'moving_images'
include MovingImages
include CommandModule

closeall_command = Command.new(:closeall)
puts JSON.pretty_generate(closeall_command.commandhash)

get_numberofobjects_command = CommandModule.make_get_nonobjectproperty(property: :numberofobjects)
puts JSON.pretty_generate(get_numberofobjects_command.commandhash)
