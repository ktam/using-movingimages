require 'moving_images'
include MovingImages

include CommandModule

the_commands = SmigCommands.new
bitmap_object = the_commands.make_createbitmapcontext(addtocleanup: false)
giffile_path = File.expand_path("~/example.gif")
exporter_object = the_commands.make_createexporter(giffile_path, addtocleanup: false, export_type: 'com.compuserve.gif')
Smig.perform_commands(the_commands)
addimage_command = CommandModule.make_addimage(exporter_object, bitmap_object)
Smig.perform_command(addimage_command)
rectangle = MIShapes.make_rectangle(size: MIShapes.make_size(800, 600))
lightgray = MIColor.make_rgbacolor(0.7,0.7,0.7)
darkgray = MIColor.make_rgbacolor(0.3,0.3,0.3)
black = MIColor.make_rgbacolor(0,0,0)
fill_rectangle = MIDrawElement.new(:fillrectangle)
fill_rectangle.fillcolor = lightgray
fill_rectangle.rectangle = rectangle
draw_command = CommandModule.make_drawelement(bitmap_object, drawinstructions: fill_rectangle)
Smig.perform_command(draw_command)
Smig.perform_command(addimage_command)
fill_rectangle.fillcolor = darkgray
Smig.perform_command(draw_command)
Smig.perform_command(addimage_command)
fill_rectangle.fillcolor = black
Smig.perform_command(draw_command)
Smig.perform_command(addimage_command)

# Objects have been created, images have been added to the image exporter object
# Now add properties

giffile_properties = { LoopCount: 0 }
dictionaryfile_properties = { '{GIF}' => giffile_properties }
add_giffileproperty_command = CommandModule.make_set_objectproperty(exporter_object, propertykey: :dictionary, propertyvalue: dictionaryfile_properties)
puts "==== The add gif file property command ===="
puts JSON.pretty_generate(add_giffileproperty_command.commandhash)

# The add giffile properties command has been created, now run it.
Smig.perform_command(add_giffileproperty_command)

# Now query the exporter object using get_objectproperty and get_objectproperties command

get_objectproperty_command = CommandModule.make_get_objectproperty(exporter_object, property: :dictionary)
dictionary_hash = JSON.parse(Smig.perform_command(get_objectproperty_command))
puts "==== The dictionary.{GIF} file property ===="
puts JSON.pretty_generate(dictionary_hash)
get_objectproperties_command = CommandModule.make_get_objectproperties(exporter_object)
properties_hash = JSON.parse(Smig.perform_command(get_objectproperties_command))
puts "==== The export file property dictionary ===="
puts JSON.pretty_generate(properties_hash)

# Now use the dictionary key path to get just the GIF part of the properties

get_objectgifproperty = CommandModule.make_get_objectproperty(exporter_object, property: 'dictionary.{GIF}')
gifproperties_hash = JSON.parse(Smig.perform_command(get_objectgifproperty))
puts "==== The {GIF} attribute/property of the dictionary ==="
puts JSON.pretty_generate(gifproperties_hash)

# Now add gif properties to the individual frames. We have 4 of them.

gifframe_hash = { ColorModel: :Gray, :'{GIF}' => { DelayTime: 0.5 } }
add_gifframeproperty_command = CommandModule.make_set_objectproperties(exporter_object, gifframe_hash, imageindex: 0)
Smig.perform_command(add_gifframeproperty_command)

puts "==== The command for assigning properties to each frame of the GIF ===="
puts JSON.pretty_generate(add_gifframeproperty_command.commandhash)

# Should normally creating a completely new command rather than hacking with
# the add_gifframeproperty_command and resetting the imageindex. If we were
# doing this in a list of commands rather than performing them one at a time
# then we'd be in trouble as the command wouldn't be duplicated. Just 4
# references to the same command and by the time they were run all 4 would have
# a imageindex of 3.

add_gifframeproperty_command.commandhash[:imageindex] = 1
Smig.perform_command(add_gifframeproperty_command)
add_gifframeproperty_command.commandhash[:imageindex] = 2
Smig.perform_command(add_gifframeproperty_command)
add_gifframeproperty_command.commandhash[:imageindex] = 3
Smig.perform_command(add_gifframeproperty_command)
export_command = CommandModule.make_export(exporter_object)
Smig.perform_command(export_command)
Smig.close_object_nothrow(exporter_object)
Smig.close_object_nothrow(bitmap_object)
Open3.capture2("open", giffile_path)
