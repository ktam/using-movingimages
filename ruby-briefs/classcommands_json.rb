require 'moving_images'
include MovingImages
include CommandModule

command = CommandModule.make_get_nonobjectproperty(property: :numberofobjects, type: :imageimporter)
puts JSON.pretty_generate(command.commandhash)

closeall_command = Command.new(:closeall)
closeall_command.add_option(key: :objecttype, value: :imageexporter)
puts JSON.pretty_generate(closeall_command.commandhash)

get_filterlist_command = CommandModule.make_get_nonobjectproperty(property: :imagefilters, type: :imagefilterchain)
get_filterlist_command.add_option(key: :filtercategory, value: :CICategoryBlur)
puts JSON.pretty_generate(get_filterlist_command.commandhash)

# In the above I was generating the json necessary for representing the command
# The line below will return the list of filters which belong to blur category.
# MIMeta.listfilters(category: :CICategoryBlur)

create_imageimporter_command = CommandModule.make_createimporter("~/Pictures/lovely.jpg", name: :lovelyimporter)
puts JSON.pretty_generate(create_imageimporter_command.commandhash)

create_bitmapcontext_command = CommandModule.make_createbitmapcontext(width: 900, height: 600, name: :documentationcontext)
puts JSON.pretty_generate(create_bitmapcontext_command.commandhash)

create_windowcontext_command = CommandModule.make_createwindowcontext(name: :documentationwindow)
puts JSON.pretty_generate(create_windowcontext_command.commandhash)

create_imageexporter_command = CommandModule.make_createexporter("~/newlovelypicture.png", export_type: 'public.png', name: :documentationexporter)
puts JSON.pretty_generate(create_imageexporter_command.commandhash)

create_pdfcontext_command = CommandModule.make_createpdfcontext(width: 480, height: 640, filepath: "~/mynicenewpdffile.pdf", name: :documentationpdfcontext)
puts JSON.pretty_generate(create_pdfcontext_command.commandhash)

create_imagefilterchain_command = CommandModule.make_createimagefilterchain({}, name: :documentationimagefilterchain)
puts JSON.pretty_generate(create_imagefilterchain_command.commandhash)

create_calculatetextsize_command = CommandModule.make_calculategraphicsizeoftext(text: "How long is a piece of string", postscriptfontname: 'AmericanTypewriter-Condensed', fontsize: 14)
puts JSON.pretty_generate(create_calculatetextsize_command.commandhash)
puts Smig.perform_command(create_calculatetextsize_command)

