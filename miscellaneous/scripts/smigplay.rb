#!/usr/bin/env ruby

require 'Open3'
require 'JSON'
require 'SecureRandom'
require 'listen'
require_relative 'smigcommands'
require_relative 'smig'
require_relative 'midrawing'

module MovingImages

  # ==The SmigPlay module is used for playing with smig in irb
  # The SmigPlay methods are not designed for building things on top of in the
  # way that the Smig and SmigCommand methods are. SmigPlay is about playing
  # with the MovingImages launch agent. Trying things out. Actions are mostly
  # carried out immediately rather than queued up for later execution.
  # SmigPlay uses the concept of a current window or bitmap context, which is
  # the target for drawing commands etc. The same goes for an exporter object.
  # Image files are imported, drawn and then closed in the one command.
  module SmigPlay
    # A hash representing the window object to be communicated with
    @@windowObject = nil
  
    # A has representing an exporter object to be communicated with.
    @@exporterObject = nil

    # Create a window.
    # Will only allow one window or bitmap context to exist at a time.
    # @param width [Float, Fixnum] The width of the window.
    # @param height [Float, Fixnum] The height of the window.
    # @param xloc [Float, Fixnum] The position of the left edge of the window
    # @param yloc [Float, Fixnum] The position of the bottom edge of the window
    # @return [Hash] A hash representing the window object
    def self.create_window(width: 800, height:600, xloc: 100, yloc: 100)
      unless @@windowObject.nil?
        Smig.perform_command(CommandModule.make_close(@@windowObject))
        @@windowObject = nil
      end
  
      if @@windowObject.nil?
        newWindowCommand = CommandModule.make_createwindowcontext(
                                                        width: width,
                                                        height: height, 
                                                        xloc: xloc,
                                                        yloc: yloc)
        result = Smig.perform_command(newWindowCommand)
        @@windowObject = { :objectreference => result.to_i }
      end
      @@windowObject
    end

    # Create a bitmap context.
    # Will only allow one window or bitmap context to exist at a time.
    # @param width [Float, Fixnum] The width of the window.
    # @param height [Float, Fixnum] The height of the window.
    # @return [Hash] A hash representing the window object
    def self.create_bitmapcontext(width: 800, height: 600)
      unless @@windowObject.nil?
        Smig.perform_command(CommandModule.make_close(@@windowObject))
        @@windowObject = nil
      end
  
      if @@windowObject.nil?
        newWindowCommand = CommandModule.make_createbitmapcontext(
                                                            width: width,
                                                            height: height)
        result = Smig.perform_command(newWindowCommand)
        @@windowObject = { :objectreference => result.to_i }
      end
      @@windowObject
    end

    # Close  SmigPlay objects. The window, and exporter object.
    def self.close_objects()
      commands = CommandModule::SmigCommands.new()
      unless @@windowObject.nil?
        closeCommand = CommandModule.make_close(@@windowObject)
        commands.add_command(closeCommand)
      end

      unless @@exporterObject.nil?
        closeCommand = CommandModule.make_close(@@exporterObject)
        commands.add_command(closeCommand)
      end

      result = ""
      unless commands.get_commandshash.length.zero?
        result = Smig.perform_commands_nothrow(commands)
      end

      @@windowObject = nil
      @@exporterObject = nil
      result
    end

    # Create an image exporter object.
    # @param filepath [String] The path to where to save the file.
    # @param filetype [String, Symbol] The image file type to save as.
    # @return [Hash] The exporter object representation
    def self.create_imageexporter(filepath: "~/Desktop/mypic.jpeg",
                                  filetype: :"public.jpeg" )
      if @@exporterObject.nil?
        imageExporter = CommandModule.make_createexporter(filepath, 
                                                          exportType: filetype)
        result = Smig.perform_command(imageExporter)
        @@exporterObject = { :objectreference => result.to_i }
      end
      @@exporterObject
    end

    # Update the exporter object with a new file location, and file type.
    # @param filepath [String] The path to where to save the file.
    # @param filetype [String, Symbol] The image file type to save as.
    # @return [Hash] The exporter object representation
    def self.set_exporter(filepath: "~/Desktop/mypic.jpeg", 
                          filetype: nil)
      if @@exporterObject.nil?
        self.create_imageexporter(filepath: filepath, filetype: filetype)
      else
        setFilePathCommand = CommandModule.make_set_objectproperty(
                                          @@exporterObject,
                                          propertykey: :exportfilepath,
                                          propertyvalue: filepath)
        theCommands = CommandModule::SmigCommands.new
        theCommands.add_command(setFilePathCommand)
        unless filetype.nil?
          setFileTypeCommand = CommandModule.make_set_objectproperty(
                                            @@exporterObject,
                                            propertykey: :utifiletype,
                                            propertyvalue: filetype)
          theCommands.add_command(setFileTypeCommand)
        end
        Smig.perform_commands(theCommands)
      end
    end

    # Save the state of the play window to a file.
    # @param filepath [String] The path to where to save the file.
    # @param filetype [String, Symbol] The image file type to save as.
    # @return [String] Empty string indicates success otherwise an error message
    def self.save(filepath: "~/Desktop/mypic.jpeg", filetype: "public.jpeg")
      if @@windowObject.nil?
        return "No window object to save"
      end
      self.set_exporter(filepath: filepath, filetype: filetype)
      addImageCommand = CommandModule.make_addimage(@@exporterObject,
                                                    @@windowObject)
      exportCommand = CommandModule.make_export(@@exporterObject)
      smigCommands = CommandModule::SmigCommands.new
      smigCommands.add_command(addImageCommand)
      smigCommands.add_command(exportCommand)
      Smig.perform_commands(smigCommands)
    end

    # Draw a rectangle with a fill color to the play window.
    # @param rectangle [Hash] An object representing a rectangular shape.
    # @param color [Hash] The fill color with which to draw the rectangle.
    # @param radius [Float, nil] To give the rectangle a corner radius.
    # @return [String] Empty string indicates success otherwise an error message
    def self.fill_rectangle(rectangle: {}, color: {}, radius: nil)
      rectElement = nil
      if radius.nil?
        rectElement = MIDrawElement.new(:fillrectangle)
      else
        rectElement = MIDrawElement.new(:fillroundedrectangle)
        rectElement.radius =radius
      end
      rectElement.fillcolor = color
      rectElement.rectangle = rectangle
      smigDrawCommand = CommandModule.make_drawelement(@@windowObject,
                                                drawinstructions: rectElement)
      Smig.perform_command(smigDrawCommand)
    end

    # Draw a rectangle with a fill color but a bit randomized to play window.
    # @param rectangle [Hash] An object representing a rectangular shape.
    # @param color [Hash] The fill color with which to draw the rectangle.
    # @param radius [Float, nil] To give the rectangle a corner radius.
    # @return [String] Empty string indicates success otherwise an error message
    def self.randomized_fill_rectangle(rectangle: {}, color: {}, radius: nil)
      MIColor.rgbacolor_setred_toequation(color, "0.6 + 0.4 * $redrandom")
      MIColor.rgbacolor_setgreen_toequation(color, "0.2 + 0.3 * $greenrandom")
      MIShapes.rect_setwidth_toequation(rectangle, "100 + 150 * $widthrandom")
      MIShapes.rect_setheight_toequation(rectangle, "50 + 100 * $heightrandom")
      MIShapes.rect_setx_toequation(rectangle, "10 + 380 * $xrandom")
      MIShapes.rect_sety_toequation(rectangle, "10 + 250 * $yrandom")
      variables = { :redrandom => Random.rand, :greenrandom => Random.rand,
                    :widthrandom => Random.rand, :heightrandom => Random.rand,
                    :xrandom => Random.rand, :yrandom => Random.rand,
                    :radiusrandom => Random.rand }
      rectElement = MIDrawElement.new(:fillroundedrectangle)
      if radius.nil?
        rectElement.radius = "5.0 + 20.0 * $radiusrandom"
      else
        rectElement.radius = radius
      end
      rectElement.variables = theVariables
      rectElement.fillcolor = color
      rectElement.rectangle = rectangle
      smigDrawCommand = CommandModule.make_drawelement(@@windowObject,
                                              drawinstructions: rectElement)
      Smig.perform_command(smigDrawCommand)
    end

    # Create numqueues number of asynchronous commands each drawing numrects
    # This command will setup the work to be done, post the commands and
    # measure how long the work takes to be done. Measurement is done based on
    # the timing arrival of the completion files in the SmigPlayAsyncText.
    # Basically the point of this command was to determine the advantage of
    # multiple queues for rendering rectangles. No disk access etc.
    # @param numqueues [Fixnum] The number of asynchronous work queues to create
    # @param numrects [Fixnum] The number of rects to draw in each work queue
    # @return nil
    def self.number_asynchronous_fillrects_commands(numqueues: 4,
                                                    numrects: 2000)
      contextsAndPaths = []
      newBitmapContextCommand = CommandModule.make_createbitmapcontext(
                                                    width: 800, height: 600)
      directoryPath = File.join(Dir.home, "Desktop/SmigPlayAsyncTest")
      unless File.directory?(directoryPath)
        FileUtils.mkdir(directoryPath)
      end

      numqueues.times do
        result = Smig.perform_command(newBitmapContextCommand)
        filePath = File.join(directoryPath, "#{SecureRandom.uuid}.json")
        theReceiver = { :objectreference => result.to_i }
        theCommand = self.create_randomized_fillrects_command(
                                  numrects: numrects,
                                  receiver: theReceiver)
        contextsAndPaths.push(
                  { :receiver => theReceiver,
                    :path =>filePath, :command => theCommand } )
      end

      startTime = Time.now
      listener = Listen.to(directoryPath) do | modified, added, removed |
        unless added.length.zero?
          added.each { |addedItem|
            puts "Added item: #{addedItem}"
            contextsAndPaths.delete_if { |contextAndPath|
              addedItem.eql? contextAndPath[:path]
            }
            puts "contextsAndPaths.length: " + contextsAndPaths.length.to_s
          }
        end
        if contextsAndPaths.length.zero?
          puts "Time to process: #{Time.now - startTime} in seconds"
          listener.stop
        end
      end

      startTime = Time.now
      listener.start
      contextsAndPaths.each { |contextAndPath|
        commands = CommandModule::SmigCommands.new
        commands.set_run_asynchronously(true)
        commands.set_informationreturned(:lastcommandresult)
        commands.set_saveresultstype(:jsonfile)
        commands.set_saveresultsto(contextAndPath[:path])
        commands.add_command(contextAndPath[:command])
        closeCommand = CommandModule.make_close(contextAndPath[:receiver])
        commands.add_command(closeCommand)
        Smig.perform_commands(commands)
      }
      nil
    end

    # Create a command that draws randomized fill rects.
    # @param numrects [Fixnum] The number of rectangles to draw
    # @param receiver [Hash] Optional the object to receive the command.
    # @return [Hash] The fill command.
    def self.create_randomized_fillrects_command(numrects: 1000, receiver: nil)
      rectangle = MIShapes.make_rectangle(size: { :width => 200.0,
                                                  :height =>150.0 })
      color = MIColor.make_rgbacolor(0.8, 0.2, 0.1)
      MIColor.rgbacolor_setred_toequation(color, "0.6 + 0.4 * $redrandom")
      MIColor.rgbacolor_setgreen_toequation(color, "0.2 + 0.3 * $greenrandom")
      MIShapes.rect_setwidth_toequation(rectangle, "100 + 150 * $widthrandom")
      MIShapes.rect_setheight_toequation(rectangle, "50 + 100 * $heightrandom")
      MIShapes.rect_setx_toequation(rectangle, "10 + 380 * $xrandom")
      MIShapes.rect_sety_toequation(rectangle, "10 + 250 * $yrandom")
      radiusFormula   = "5.0 + 20 * $radiusrandom"
      arrayOfElements = MIDrawElement.new(:arrayofelements)
      numrects.times do
        theVariables = { :redrandom => Random.rand, :greenrandom =>Random.rand,
                    :widthrandom => Random.rand, :heightrandom => Random.rand,
                    :xrandom => Random.rand, :yrandom => Random.rand,
                    :radiusrandom => Random.rand }
        rectElement = MIDrawElement.new(:fillroundedrectangle)
        rectElement.radius radiusFormula
        rectElement.variables = theVariables
        rectElement.fillcolor = color
        rectElement.rectangle = rectangle
        arrayOfElements.add_drawelement_toarrayofelements(rectElement)
      end
      theReceiver = receiver unless receiver.nil?
      theReceiver = @@windowObject if receiver.nil?
      smigDrawCommand = CommandModule.make_drawelement(theReceiver,
                                              drawinstructions: arrayOfElements)
      smigDrawCommand
    end

    # Create a fill rectangle command using constant values plus random values
    # @return [Hash] The fill command.
    def self.randomized_create_fillcommand()
      rectangle = MIShapes.make_rectangle(size: { :width => 200.0,
                                                  :height =>150.0 })
      radius = nil
#      radius = 25
      color = MIColor.make_rgbacolor(0.8, 0.2, 0.1)
      MIColor.rgbacolor_setred_toequation(color, "0.6 + 0.4 * $redrandom")
      MIColor.rgbacolor_setgreen_toequation(color, "0.2 + 0.3 * $greenrandom")
      MIShapes.rect_setwidth_toequation(rectangle, "100 + 150 * $widthrandom")
      MIShapes.rect_setheight_toequation(rectangle, "50 + 100 * $heightrandom")
      MIShapes.rect_setx_toequation(rectangle, "10 + 380 * $xrandom")
      MIShapes.rect_sety_toequation(rectangle, "10 + 250 * $yrandom")
      theVariables = { :redrandom => Random.rand, :greenrandom => Random.rand,
                  :widthrandom => Random.rand, :heightrandom => Random.rand,
                  :xrandom => Random.rand, :yrandom => Random.rand,
                  :radiusrandom => Random.rand }
      rectElement = MIDrawElement.new(:fillroundedrectangle)
      if radius.nil?
        rectElement.radius = "5.0 + 20.0 * $radiusrandom"
      else
        rectElement.radius = radius
      end
      rectElement.variables = theVariables
      rectElement.fillcolor = color
      rectElement.rectangle = rectangle
      smigDrawCommand = CommandModule.make_drawelement(@@windowObject,
                                                drawinstructions: rectElement)
      smigDrawCommand
    end

    # Close the exporter object
    def self.close_exporter()
        closeCommand = CommandModule.make_close(@@exporterObject)
        Smig.perform_command_nothrow(closeCommand)
        @@exporterObject = nil
    end
  end
end

=begin

The following demonstrates the use of many of the play methods implemented
in this file. 

Just don't call all the number_asynchronous_fillrects_commands methods
in one go.

theRect = MIShapes.make_rectangle(size: { :width => 200.0, :height =>150.0 })
theColor =  MIColor.make_rgbacolor(0.8, 0.2, 0.1)
SmigPlay.create_window(width: 1200, height: 800)
SmigPlay.fill_rectangle(rectangle: theRect, color: theColor, radius: 15)
SmigPlay.save(filepath: "~/Desktop/mypic.jpeg", filetype: "public.jpeg")
SmigPlay.close_objects()

SmigPlay.create_window()
theCommands = CommandModule::SmigCommands.new
1500.times { theCommands.add_command(SmigPlay.randomized_create_fillcommand()) }
Smig.perform_timed_commands(theCommands) # Time to run commands: 25.3 seconds 14% CPU usage
Smig.perform_timed_commands(theCommands, debug: true) # Time to run commands: 25.4 seconds 14% CPU usage

theCommands.clear()
SmigPlay.create_bitmapcontext()
1500.times { theCommands.add_command(SmigPlay.randomized_create_fillcommand()) }
Smig.perform_timed_commands(theCommands) # Time to run commands: 1.96 seconds 100% CPU usage
Smig.perform_timed_commands(theCommands, debug: true) # Time to run commands: 12 seconds 23% CPU usage

theCommands.clear()
theCommands.add_command(SmigPlay.create_randomized_fillrects_command(numrects: 1500))
Smig.perform_timed_commands(theCommands) # Time to run commands: 1.8 seconds 100% CPU usage

SmigPlay.create_window()
theCommands.clear()
theCommands.add_command(SmigPlay.create_randomized_fillrects_command(numrects: 1500))
Smig.perform_timed_commands(theCommands) # Time to run commands: 1.85 seconds
SmigPlay.close_objects()

SmigPlay.number_asynchronous_fillrects_commands(numqueues: 3, numrects: 21000) # 44 seconds. 300% CPU
SmigPlay.number_asynchronous_fillrects_commands(numqueues: 4, numrects: 16000) # 33 seconds. 400% CPU
SmigPlay.number_asynchronous_fillrects_commands(numqueues: 5, numrects: 12800) # 35 seconds. 490% CPU
SmigPlay.number_asynchronous_fillrects_commands(numqueues: 6, numrects: 10666) # 36 seconds. 550% CPU
SmigPlay.number_asynchronous_fillrects_commands(numqueues: 8, numrects: 8000) # 39 seconds. 670% CPU

== Drawing to a window:
Requesting a nsgraphicscontexts before drawing each shape, 50 rectangles drawn a second approx.

Drawing a rounded rectangle with formulas & variables for dimensions, positions and color components 800 rectangles drawn a second.

Drawing a rounded rectangle size 200x150, no formulas: 7000 rectangles a second

Drawing a rectangle size 200x150, no formulas: 10000 rectangles a second.

== Drawing to a bitmap context:
Drawing a rounded rectangle with formulas & variables for dimensions, positions and color components 2000 rectangles drawn a second. (4 different bitmap contexts running async - this is maximum throughput)

Drawing a rectangle size 200x150, no formulas: 11000 rectangles a second. One bitmapcontext no async.

As soon as equations not needed ruby can't provide data to movingimages fast enough for moving images to process when running async processes with multiple bitmaps. The listener has already returned before the next command can be sent.

=end
