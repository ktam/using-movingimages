# @markup markdown
# @title MovingImage ruby interface reference
# @author Kevin Meaney
# @copyright Kevin Meaney (MIT license)

### The ruby interface for Moving Images

MovingImages: An OSX LaunchAgent that is a scriptable image processing and drawing tool for OSX

[The using the ruby interface to accessing MovingImages](https://gitlab.com/ktam/using-movingimages/wikis/Contents).

This reference documentation for the ruby interface to MovingImages describes the Modules, Classes, class and object methods for driving MovingImages.

I'm happy for this documentation to be duplicated and or modified for your own use, but I require acknowledgement with a link back to the original source.

Kevin Meaney
