require 'moving_images'
include MovingImages
include MICGDrawing
include MIMovie
include CommandModule

def TestProcessingMovieFramePerformance()
#  MILAMeta.idletime = 300
  startRunTime = Time.now
  sourceMovie = "/Users/ktam/Movies/604_sd_clip.mov"
  pagesFolder = File.expand_path("~/Desktop/pages")
  FileUtils.mkdir_p(pagesFolder)

  width = 576
  height = 360
  movieLength = 300 # seconds.

  # These are configurable so it is possible to play with the frame duration
  # whilst still getting the same number of frames.
  # ============================
  startTime = 0.0 # movieLength * 0.7 # For testing vary between 0.0 and 1.0.
  # ============================
  endTime = movieLength

  numFrames = 1440
  numWorkGroups = 12
  numFramesPerWorkGroup = numFrames / numWorkGroups
  
  frameDuration = (endTime - startTime) / numFrames.to_f
  
  borderWidth = 32
  bitmapWidth = (3.0 * width.to_f * 0.5 + (3+1) * borderWidth).to_i
  bitmapHeight = (4.0 * height.to_f * 0.5 + (4+1) * borderWidth).to_i
  bitmapSize = MIShapes.make_size(bitmapWidth, bitmapHeight)
  baseFileName = "coversheet"

  begin
   # Create movie importer command & add it to the list of commands.
   # The movie importer object is not automatically closed in the list of
   # cleanup commands so we need to remember to close it manually at the end.
   movieImporterName = SecureRandom.uuid
   createMovieImporterCommand = CommandModule.make_createmovieimporter(
                                     sourceMovie, name: movieImporterName)
   movieObject = SmigIDHash.make_objectid(objecttype: :movieimporter,
                                          objectname: movieImporterName)
   Smig.perform_command(createMovieImporterCommand)

    workGroupDuration = (endTime - startTime) / numWorkGroups.to_f
    pageNumber = 0
    numWorkGroups.times do |workGroupIndex|
      workGroupStartTime = startTime + workGroupDuration * workGroupIndex.to_f
      workGroupEndTime = workGroupStartTime + workGroupDuration

      # Create the list of commands object
      theCommands = SmigCommands.new
      theCommands.saveresultstype = :lastcommandresult

      # Basically make all processing asynchronous except for last work group.
      # This means the script won't finish until last workgroup has completed.
      # There is a small chance the second to last workgroup may not have
      # finished, but it will do so shortly after.
      unless workGroupIndex + 1 == numWorkGroups
        theCommands.run_asynchronously = true
      end

      # 3. Create the process movie frames command and configure.
      imageIdentifier = SecureRandom.uuid

      processFramesCommand = ProcessFramesCommand.new(movieObject)
      processFramesCommand.create_localcontext = true
      processFramesCommand.imageidentifier = imageIdentifier
      track_id = MovieTrackIdentifier.make_movietrackid_from_mediatype(
                                               mediatype: :vide, trackindex: 0)

      processFramesCommand.videotracks = [ track_id ]

      # 4. Make a pre-process commad list.
      preProcessCommands = []
    
      # 5. Make a create a bitmap context command.
      bitmapName = SecureRandom.uuid
      createBitmapCommand = CommandModule.make_createbitmapcontext(
                                            name: bitmapName, size: bitmapSize)

      bitmapObject = SmigIDHash.make_objectid(objecttype: :bitmapcontext,
                                              objectname: bitmapName)

      # 6. Add the create bitmap context object command to the pre-process list.
      preProcessCommands.push(createBitmapCommand.commandhash)

      # 7. Make a create exporter command & add it to the pre-process list
      exporterName = SecureRandom.uuid
      createExporterCommand = CommandModule.make_createexporter(
          "~/placeholder.jpg", export_type: 'public.jpeg', name: exporterName)
      preProcessCommands.push(createExporterCommand.commandhash)
      exporterObject = SmigIDHash.make_objectid(objecttype: :imageexporter,
                                                objectname: exporterName)

      # 8. Assign the pre-process commands to the process movie frames command.
      processFramesCommand.preprocesscommands = preProcessCommands
    
      # 9. Add a close bitmap object command to cleanup commands.
      processFramesCommand.add_tocleanupcommands_closeobject(bitmapObject)
    
      # 10. Add a close exporter object command to cleanup commands.
      processFramesCommand.add_tocleanupcommands_closeobject(exporterObject)

      # 11. Add a remove image from collection command to cleanup commands.
      processFramesCommand.add_tocleanupcommands_removeimage(imageIdentifier)

      # 12. Prepare and start looping for creating process frame instrutions.
      framesPerPage = 12 # 3 x 4
      x = 0
      y = 0
      halfWidth = width / 2
      halfHeight = height / 2
      drawnFrameSize = MIShapes.make_size(halfWidth, halfHeight)
      textBoxSize = MIShapes.make_size(halfWidth, borderWidth * 3 / 4)
      filesToCompare = []
      numFramesPerWorkGroup.times do |i|
        # 13. Create a ProcessMovieFrameInstruction object
        frameInstructions = ProcessMovieFrameInstructions.new
      
        # 14. Calculate the frame time and assign it.
        time = workGroupStartTime + i.to_f * frameDuration
        frameTime = MovieTime.make_movietime_fromseconds(time)
        frameInstructions.frametime = frameTime
      
        # 15. Determine the frame number on the page & destination rectangle.
        frameNumber = i % framesPerPage
        x = frameNumber % 3
        y = 3 - (frameNumber / 3)
        xloc = x * halfWidth + (x + 1) * borderWidth
        yloc = y * halfHeight + (y + 1) * borderWidth
        origin = MIShapes.make_point(xloc, yloc)
        drawnFrameRect = MIShapes.make_rectangle(size: drawnFrameSize,
                                               origin: origin)
      
        # 16. Create the draw image element to draw the frame onto the bitmap.
        drawImageElement = MIDrawImageElement.new()
        drawImageElement.destinationrectangle = drawnFrameRect
        drawImageElement.set_imagecollection_imagesource(
                                                   identifier: imageIdentifier)
      
        # 17. Create the draw image command and add it to the frame instructions
        drawImageCommand = CommandModule.make_drawelement(bitmapObject,
                        drawinstructions: drawImageElement, createimage: false)
        frameInstructions.add_command(drawImageCommand)

# =begin # Don't draw the text. Irrelevant to movie frame drawing.
        # 18. Prepare drawing the text with the time.
        timeString = "Frame time: %.3f secs" % time
        drawStringElement = MIDrawBasicStringElement.new()
        drawStringElement.stringtext = timeString
        drawStringElement.userinterfacefont = "kCTFontUIFontLabel"
        drawStringElement.textalignment = "kCTTextAlignmentCenter"
        drawStringElement.fillcolor = MIColor.make_rgbacolor(0.0, 0.0, 0.0)
        boxOrigin = MIShapes.make_point(xloc, yloc - borderWidth)
        boundingBox = MIShapes.make_rectangle(size: textBoxSize,
                                            origin: boxOrigin)
        drawStringElement.boundingbox = boundingBox
        drawTextCommand = CommandModule.make_drawelement(bitmapObject,
                        drawinstructions: drawStringElement, createimage: false)
        frameInstructions.add_command(drawTextCommand)
# =end

        # 19. If this was the last frame to be drawn then export the page.
        if (frameNumber == framesPerPage - 1) || i == numFrames - 1
          addImageCommand = CommandModule.make_addimage(exporterObject,
                                                        bitmapObject)
          frameInstructions.add_command(addImageCommand)
          pageNum = pageNumber.to_s.rjust(3, '0')
          fileName = baseFileName + pageNum + ".jpg"
          filesToCompare.push(fileName)
          
          filePath = File.join(pagesFolder, fileName)
          setExportPathCommand = CommandModule.make_set_objectproperty(
                        exporterObject, propertykey: :exportfilepath,
                                      propertyvalue: filePath)
          frameInstructions.add_command(setExportPathCommand)
          exportCommand = CommandModule.make_export(exporterObject)
          frameInstructions.add_command(exportCommand)
          # 20. Now redraw the bitmap context with a white rectangle.
          redrawBitmapElement = MIDrawElement.new(:fillrectangle)
          redrawBitmapElement.fillcolor = MIColor.make_rgbacolor(1.0, 1.0, 1.0)
          redrawBitmapElement.rectangle = MIShapes.make_rectangle(
                                                              size: bitmapSize)
          redrawCommand = CommandModule.make_drawelement(bitmapObject,
                                       drawinstructions: redrawBitmapElement)
          frameInstructions.add_command(redrawCommand)
          pageNumber += 1
        end
        # 21. Set the frame processing intructions to the process frames command
        processFramesCommand.add_processinstruction(frameInstructions)
      end # numFramesPerWorkGroup.times do

      # 22. Add the process frames command to the list of commands.
      theCommands.add_command(processFramesCommand)
      Smig.perform_commands(theCommands)
      puts "Work group commands with index #{workGroupIndex} posted"
    end # numWorkGroups.times do
  ensure
    Smig.close_object_nothrow(movieObject)
  end # begin
  time_to_process = Time.now - startRunTime
  time_to_process = sprintf "%.3f", time_to_process
  puts "Time to process: #{time_to_process} in seconds"
end

TestProcessingMovieFramePerformance()
