require 'moving_images'
# require 'listen'

include MovingImages

#  These tests assume the image files supplied all have the same dimensions.
# 1. test speed of importing image files.
# 2. test speed of drawing images to a bitmap context.
# 3. test speed of scaled drawing of images to a window context. (908, 681)
# 4. test speed of scaled drawing of images to a bitmap context.
#      different quality options
# 5. test speed of scaled drawing of image using CILanczosFilter
# 6. test speed of exporting
#      jpeg, tiff, png
# 7. test speed of exporting asynchronously.
#      will need to check for creation of files.
# 8. Need to test running of multiple command queues asynchronously.

module MovingImagesPerformance
  def self.import_imagefilelist(smig_commands, image_filelist)
    image_filelist.each do |imagefile_path|
      smig_commands.make_createimporter(imagefile_path)
    end
  end

  def self.drawimages_tobitmap(smig_commands, image_filelist,
                                      preset: :AlphaPreMulFirstRGB8bpcInt)
    dimensions = SpotlightCommand.get_imagedimensions(image_filelist[0])
    bitmap_object = smig_commands.make_createbitmapcontext(size: dimensions,
                                                           preset: preset)
    draw_rect = MIShapes.make_rectangle(size: dimensions)
    image_filelist.each do |imagefile_path|
      importer_object = smig_commands.make_createimporter(imagefile_path)
      draw_imageelement = MIDrawImageElement.new
      draw_imageelement.destinationrectangle = draw_rect
      draw_imageelement.set_imagefile_imagesource(
                                     source_object: importer_object,
                                        imageindex: 0)
      draw_command = CommandModule.make_drawelement(bitmap_object,
                                          drawinstructions: draw_imageelement)
      smig_commands.add_command(draw_command)
    end
  end

  def self.scaleimages_towindow(smig_commands, image_filelist,
                          window_dimensions: { width: 908, height: 681 })
    image_dimensions = SpotlightCommand.get_imagedimensions(image_filelist[0])
    source_rect = MIShapes.make_rectangle(size: image_dimensions)
    window_rect = MIShapes.make_rectangle(size: window_dimensions,
                                          origin: { x: 200, y: 100 })
    dest_rect = MIShapes.make_rectangle(size: window_dimensions)
    window_object = smig_commands.make_createwindowcontext(rect: window_rect)
    image_filelist.each do |imagefile_path|
      importer_object = smig_commands.make_createimporter(imagefile_path)
      draw_imageelement = MIDrawImageElement.new
      draw_imageelement.destinationrectangle = dest_rect
      draw_imageelement.interpolationquality = :kCGInterpolationDefault
      draw_imageelement.set_imagefile_imagesource(
                                     source_object: importer_object,
                                        imageindex: 0)
      draw_command = CommandModule.make_drawelement(window_object,
                                          drawinstructions: draw_imageelement)
      smig_commands.add_command(draw_command)
    end
  end

  def self.scaleimages_tobitmap(smig_commands, image_filelist,
                          interpolationquality: :kCGInterpolationMedium,
                          bitmap_dimensions: { width: 908, height: 681 })
    image_dimensions = SpotlightCommand.get_imagedimensions(image_filelist[0])
    source_rect = MIShapes.make_rectangle(size: image_dimensions)
    dest_rect = MIShapes.make_rectangle(size: bitmap_dimensions)
    bitmap_object = smig_commands.make_createbitmapcontext(
                                                    size: bitmap_dimensions)
    image_filelist.each do |imagefile_path|
      importer_object = smig_commands.make_createimporter(imagefile_path)
      draw_imageelement = MIDrawImageElement.new
      draw_imageelement.destinationrectangle = dest_rect
      draw_imageelement.interpolationquality = interpolationquality
      draw_imageelement.set_imagefile_imagesource(
                                     source_object: importer_object,
                                        imageindex: 0)
      draw_command = CommandModule.make_drawelement(bitmap_object,
                                          drawinstructions: draw_imageelement)
      smig_commands.add_command(draw_command)
    end
  end

  def self.scaleimage_tobitmap(smig_commands, image_file,
                          interpolationquality: :kCGInterpolationMedium,
                          bitmap_dimensions: { width: 908, height: 681 },
                          numtimes: 20)
    image_dimensions = SpotlightCommand.get_imagedimensions(image_file)
    source_rect = MIShapes.make_rectangle(size: image_dimensions)
    dest_rect = source_rect
    bitmap_object = smig_commands.make_createbitmapcontext(
                                                    size: bitmap_dimensions)
    importer_object = smig_commands.make_createimporter(image_file)
    draw_imageelement = MIDrawImageElement.new
    draw_imageelement.destinationrectangle = dest_rect
    draw_imageelement.interpolationquality = interpolationquality
    context_transforms = MITransformations.make_contexttransformation
    MITransformations.add_scaletransform(context_transforms,
                                         MIShapes.make_point(0.4, 0.4))
    draw_imageelement.contexttransformations = context_transforms
    draw_imageelement.set_imagefile_imagesource(
                                   source_object: importer_object,
                                      imageindex: 0)
    draw_command = CommandModule.make_drawelement(bitmap_object,
                                        drawinstructions: draw_imageelement)
    numtimes.times { smig_commands.add_command(draw_command) }
  end
end

image_file_folder = File.expand_path("~/Pictures/aatemp")
# image_file_folder = MILibrary::Utility.select_a_folder
the_width = 2272
the_height = 1704
image_files = SpotlightCommand.find_imagefiles_withdimensions(
                 width: the_width, height: the_height, 
                 filetype: 'public.image', onlyin: image_file_folder)

smig_commands = CommandModule::SmigCommands.new

MovingImagesPerformance.import_imagefilelist(smig_commands, image_files[:files])
puts "============================="
puts "Number image files imported: #{image_files[:files].length}"
time = Smig.perform_timed_commands(smig_commands)
time = sprintf "%.4f", time
puts "Took #{time} seconds to import and close #{image_files[:files].length}"

subset_images = image_files[:files][0..19]
smig_commands = CommandModule::SmigCommands.new
MovingImagesPerformance.drawimages_tobitmap(smig_commands, subset_images)
puts "============================="
puts "Number image files drawn to ARGBI8bpc bitmap: #{subset_images.length}"
time = Smig.perform_timed_commands(smig_commands)
rate = sprintf "%.2f", subset_images.length.to_f / time
time = sprintf "%.4f", time
puts "Took #{time} seconds to draw: #{subset_images.length}"
puts "At a rate of #{rate} image draws / per second"

smig_commands = CommandModule::SmigCommands.new
MovingImagesPerformance.drawimages_tobitmap(smig_commands, subset_images,
                                    preset: :AlphaPreMulLastRGB32bpcFloat)
puts "============================="
puts "Number image files drawn to RGBAF32bpc bitmap: #{subset_images.length}"
time = Smig.perform_timed_commands(smig_commands)
rate = sprintf "%.2f", subset_images.length.to_f / time
time = sprintf "%.4f", time
puts "Took #{time} seconds to draw: #{subset_images.length}"
puts "At a rate of #{rate} image draws / per second"

smig_commands = CommandModule::SmigCommands.new
MovingImagesPerformance.scaleimages_towindow(smig_commands, subset_images,
                          window_dimensions: { width: 908, height: 681 })
start_time = Time.now
Smig.perform_debugcommands(smig_commands)
time = Time.now - start_time
rate = sprintf "%.2f", subset_images.length.to_f / time
time = sprintf "%.4f", time
puts "============================="
puts "Number images scaled to fit window(908, 681) is #{subset_images.length}"
puts "Took #{time} secs to scale images at medium quality #{rate} per sec"

smig_commands = CommandModule::SmigCommands.new
MovingImagesPerformance.scaleimages_tobitmap(smig_commands, subset_images,
                              bitmap_dimensions: { width: 908, height: 681 },
                              interpolationquality: :kCGInterpolationMedium)
time = Smig.perform_timed_commands(smig_commands)
rate = sprintf "%.2f", subset_images.length.to_f / time
time = sprintf "%.4f", time
puts "============================="
puts "Number images scaled to fit bitmap(908, 681) is #{subset_images.length}"
puts "Took #{time} secs to scale images at medium quality #{rate} per sec"

smig_commands = CommandModule::SmigCommands.new
MovingImagesPerformance.scaleimage_tobitmap(smig_commands,
                          image_files[:files].last,
                          interpolationquality: :kCGInterpolationLow,
                          bitmap_dimensions: { width: 908, height: 681 },
                          numtimes: 60)
time = Smig.perform_timed_commands(smig_commands)
rate = sprintf "%.2f", 60.0 / time
time = sprintf "%.4f", time
puts "============================="
puts "Image scaled to fit bitmap(908, 681) 60 times"
puts "Took #{time} secs to scale image 60 times at low quality #{rate} per sec"

smig_commands = CommandModule::SmigCommands.new
MovingImagesPerformance.scaleimage_tobitmap(smig_commands,
                          image_files[:files].last,
                          interpolationquality: :kCGInterpolationMedium,
                          bitmap_dimensions: { width: 908, height: 681 },
                          numtimes: 60)
time = Smig.perform_timed_commands(smig_commands)
rate = sprintf "%.2f", 60.0 / time
time = sprintf "%.4f", time
puts "============================="
puts "Image scaled to fit bitmap(908, 681) 60 times"
puts "Took #{time} secs to scale image 60 times at med quality #{rate} per sec"

smig_commands = CommandModule::SmigCommands.new
MovingImagesPerformance.scaleimage_tobitmap(smig_commands,
                          image_files[:files].last,
                          interpolationquality: :kCGInterpolationHigh,
                          bitmap_dimensions: { width: 908, height: 681 },
                          numtimes: 60)
time = Smig.perform_timed_commands(smig_commands)
rate = sprintf "%.2f", 60.0 / time
time = sprintf "%.4f", time
puts "============================="
puts "Image scaled to fit bitmap(908, 681) 60 times"
puts "Took #{time} secs to scale image 60 times at high quality #{rate} per sec"

# if :exportfiletype is nil, then the export file type will be the same as
# the import file type.
the_opts = { outputdir: '~/Desktop/performance', scalex: 0.4, scaley: 0.4,
              copymetadata: false, interpqual: :medium, quality: 0.7,
              exportfiletype: nil, async: false, savejsonfileto: nil }

start_time = Time.now
MILibrary.scale_files_usequartz(the_opts, { files: subset_images,
                                  width: the_width, height: the_height })
end_time = Time.now
time = end_time - start_time
rate = sprintf "%.2f", subset_images.length.to_f/time
time = sprintf "%.4f", time
puts "============================="
puts "#{subset_images.length} images scaled and exported to bitmap(908, 681)"
puts "Export compression quality: 0.7"
puts "Took #{time} secs to scale #{subset_images.length} images at med quality"\
     " #{rate} per sec"

the_opts = { outputdir: '~/Desktop/performance', scalex: 0.4, scaley: 0.4,
              copymetadata: false, interpqual: :high, quality: 0.7,
              exportfiletype: nil, async: false, savejsonfileto: nil }

start_time = Time.now
MILibrary.scale_files(the_opts, { files: subset_images, width: the_width,
                                  height: the_height })
end_time = Time.now
time = end_time - start_time
rate = sprintf "%.2f", subset_images.length.to_f/time
time = sprintf "%.4f", time
puts "============================="
puts "#{subset_images.length} images scaled and exported to bitmap(908, 681)"
puts "Export compression quality: 0.7"
puts "Took #{time} secs to scale #{subset_images.length} images. High quality"\
     " #{rate} per sec."

the_opts = { outputdir: '~/Desktop/performance', scalex: 0.4, scaley: 0.4,
              copymetadata: false, interpqual: :high, quality: 0.95,
              exportfiletype: nil, async: false, savejsonfileto: nil }

start_time = Time.now
MILibrary.scale_files(the_opts, { files: subset_images, width: the_width,
                                  height: the_height } )
end_time = Time.now
time = end_time - start_time
rate = sprintf "%.2f", subset_images.length.to_f/time
time = sprintf "%.4f", time
puts "============================="
puts "#{subset_images.length} images scaled and exported to bitmap(908, 681)"
puts "Export compression quality: 0.95"
puts "Took #{time} secs to scale #{subset_images.length} images. High quality"\
     " #{rate} per sec."

the_opts = { outputdir: '~/Desktop/performance', scalex: 0.4, scaley: 0.4,
              copymetadata: false, quality: 0.95, interpqual: :lanczos,
              exportfiletype: nil, async: false, savejsonfileto: nil }

start_time = Time.now
MILibrary.scale_files(the_opts, { width: the_width, height: the_height, 
                                  files: subset_images } )
end_time = Time.now
time = end_time - start_time
rate = sprintf "%.2f", subset_images.length.to_f/time
time = sprintf "%.4f", time
puts "============================="
puts "#{subset_images.length} images scaled and exported to bitmap(908, 681)"
puts "Export compression quality: 0.95"
puts "Took #{time} secs to scale #{subset_images.length} images using Lanczos "\
     "#{rate} per sec."

the_opts = { outputdir: '~/Desktop/performance', scalex: 0.4, scaley: 0.4,
              copymetadata: false, quality: 0.95, interpqual: :lanczos,
              exportfiletype: nil, async: false, savejsonfileto: nil,
              softwarerender: true }

start_time = Time.now
MILibrary.scale_files(the_opts, { width: the_width, height: the_height, 
                                  files: subset_images } )
end_time = Time.now
time = end_time - start_time
rate = sprintf "%.2f", subset_images.length.to_f/time
time = sprintf "%.4f", time
puts "============================="
puts "#{subset_images.length} images scaled and exported to bitmap(908, 681)"
puts "Export compression quality: 0.95"
puts "Took #{time} secs to scale #{subset_images.length} images using Lanczos "\
     "#{rate} per sec. Software render."


def make_options(outputdir: '~/Desktop/performance', scalex: 0.4, scaley: 0.4,
              copymetadata: false, quality: 0.7, interpqual: :medium,
              exportfiletype: :'public.jpeg', async: false,
              savejsonfileto: nil)
  the_opts = {}
  the_opts[:outputdir] = outputdir
  the_opts[:scalex] = scalex
  the_opts[:scaley] = scaley
  the_opts[:copymetadata] = copymetadata
  the_opts[:quality] = quality
  the_opts[:interpqual] = interpqual
  the_opts[:exportfiletype] = exportfiletype
  the_opts[:async] = async
  the_opts[:savejsonfileto] = savejsonfileto
  the_opts
end

num_queues = `sysctl hw.physicalcpu`
num_queues = num_queues.split(" ")[1].to_i + 2
# num_queues = 6
results_dir = File.expand_path('~/Desktop/smig_performance/')
FileUtils.mkdir_p(results_dir)

results_array = []
listof_list_offiles = []
num_queues.times do |queue_index|
  results_file = File.join(results_dir, "#{SecureRandom.uuid}.json")
  results_array.push(results_file)
  file_list = []
  image_files[:files].each_index do |i|
    file_list.push(image_files[:files][i]) if (i % num_queues).eql? queue_index
  end
  listof_list_offiles.push(file_list)
end

=begin
num_items = results_array.size

startTime = Time.now
listener = Listen.to(results_dir) do | modified, added, removed |
  unless added.length.zero?
    added.each do |added_item|
      if results_array.member?(added_item)
        num_items = num_items.pred
      end
    end
  end
  if num_items.zero?
    total_processed_images = image_files[:files].length
    time_to_process = Time.now - startTime
    rate_drawn = (total_processed_images/time_to_process).to_i
    time_to_process = sprintf "%.3f", time_to_process
    puts "======================================"
    puts "Running #{num_queues} concurrent processing queues"
    puts "Export compression quality: 0.9, export file type jpeg"
    puts "Interpolation quality: high"
    puts "Number of image files processed #{total_processed_images}"
    puts "Time to process: #{time_to_process} in seconds"
    puts "Images processed at: #{rate_drawn}/sec"
    listener.stop
  end
end

startTime = Time.now
listener.start

num_queues.times do |queue_index|
  the_options = make_options(async: true, quality: 0.9, interpqual: :high,
                             savejsonfileto: results_array[queue_index])
  the_files = listof_list_offiles[queue_index]
  if the_files.size.eql? 0
    puts "Queue with index #{queue_index} has zero files"
  else
    if (queue_index == (num_queues -1))
      the_options[:async] = false
    end
    MILibrary.scale_files(the_options, { width: the_width, height: the_height, 
                                       files: the_files} )
  end
end

sleep(7)
listener.stop
=end

startTime = Time.now
num_queues.times do |queue_index|
  the_options = make_options(async: true, quality: 0.9, interpqual: :high,
                             savejsonfileto: results_array[queue_index])
  the_files = listof_list_offiles[queue_index]
  if the_files.size.eql? 0
    puts "Queue with index #{queue_index} has zero files"
  else
    if (queue_index == (num_queues -1))
      the_options[:async] = false
    end
    MILibrary.scale_files(the_options, { width: the_width, height: the_height, 
                                       files: the_files} )
  end
end

total_processed_images = image_files[:files].length
time_to_process = Time.now - startTime
rate_drawn = (total_processed_images/time_to_process).to_i
time_to_process = sprintf "%.3f", time_to_process
puts "======================================"
puts "Running #{num_queues} concurrent processing queues"
puts "Export compression quality: 0.9, export file type jpeg"
puts "Interpolation quality: high"
puts "Number of image files processed #{total_processed_images}"
puts "Time to process: #{time_to_process} in seconds"
puts "Images processed at: #{rate_drawn}/sec"

results_array.each { |file_path| FileUtils.rm_f(file_path) }

results_array = []
listof_list_offiles = []
num_queues.times do |queue_index|
  results_file = File.join(results_dir, "#{SecureRandom.uuid}.json")
  results_array.push(results_file)
  file_list = []
  image_files[:files].each_index do |i|
    file_list.push(image_files[:files][i]) if (i % num_queues).eql? queue_index
  end
  listof_list_offiles.push(file_list)
end

startTime = Time.now

num_queues.times do |queue_index|
  is_async = ((queue_index == num_queues - 1) ? false : true)
  the_options = make_options(async: is_async, quality: 0.9, interpqual: :high,
                          savejsonfileto: results_array[queue_index],
                          exportfiletype: :'public.png')
  the_files = listof_list_offiles[queue_index]
  MILibrary.scale_files(the_options, { width: the_width, height: the_height, 
                                       files: the_files} )
end

total_processed_images = image_files[:files].length
time_to_process = Time.now - startTime
rate_drawn = (total_processed_images/time_to_process).to_i
time_to_process = sprintf "%.3f", time_to_process
puts "======================================"
puts "Running #{num_queues} concurrent processing queues"
puts "Export compression quality: 0.9, export file type png"
puts "Interpolation quality: high"
puts "Number of image files processed #{total_processed_images}"
puts "Time to process: #{time_to_process} in seconds"
puts "Images processed at: #{rate_drawn}/sec"

results_array.each { |file_path| FileUtils.rm_f(file_path) }

results_array = []
listof_list_offiles = []
num_queues.times do |queue_index|
  results_file = File.join(results_dir, "#{SecureRandom.uuid}.json")
  results_array.push(results_file)
  file_list = []
  image_files[:files].each_index do |i|
    file_list.push(image_files[:files][i]) if (i % num_queues).eql? queue_index
  end
  listof_list_offiles.push(file_list)
end

=begin
num_items = results_array.size

startTime = Time.now
listener = Listen.to(results_dir) do | modified, added, removed |
  unless added.length.zero?
    added.each do |added_item|
      if results_array.member?(added_item)
        num_items = num_items.pred
      end
    end
  end
  if num_items.zero?
    total_processed_images = image_files[:files].length
    time_to_process = Time.now - startTime
    rate_drawn = (total_processed_images/time_to_process).to_i
    time_to_process = sprintf "%.3f", time_to_process
    puts "======================================"
    puts "Running #{num_queues} concurrent processing queues"
    puts "Export compression quality: 0.9, export file type tiff"
    puts "Interpolation quality: high"
    puts "Number of image files processed #{total_processed_images}"
    puts "Time to process: #{time_to_process} in seconds"
    puts "Images processed at: #{rate_drawn}/sec"
    listener.stop
  end
end

startTime = Time.now
listener.start
=end

startTime = Time.now
num_queues.times do |queue_index|
  the_options = make_options(async: true, quality: 0.9, interpqual: :high,
                          savejsonfileto: results_array[queue_index],
                          exportfiletype: :'public.tiff')
  if (queue_index == (num_queues -1))
    the_options[:async] = false
  end
  the_files = listof_list_offiles[queue_index]
  MILibrary.scale_files(the_options, { width: the_width, height: the_height, 
                                       files: the_files} )
end

total_processed_images = image_files[:files].length
time_to_process = Time.now - startTime
rate_drawn = (total_processed_images/time_to_process).to_i
time_to_process = sprintf "%.3f", time_to_process
puts "======================================"
puts "Running #{num_queues} concurrent processing queues"
puts "Export compression quality: 0.9, export file type tiff"
puts "Interpolation quality: high"
puts "Number of image files processed #{total_processed_images}"
puts "Time to process: #{time_to_process} in seconds"
puts "Images processed at: #{rate_drawn}/sec"

results_array.each { |file_path| FileUtils.rm_f(file_path) }
