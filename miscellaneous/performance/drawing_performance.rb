require 'moving_images'
# require 'listen'

include MovingImages

module MovingImagesPerformance
  def self.create_drawrectanglecommands(smig_commands, receiver_object,
                                                number_of_rectangles: 500)
    number_of_rectangles.times do
      drawrectangle_element = MIDrawElement.new(:fillrectangle)
      drawrectangle_element.rectangle = MIShapes.make_rectangle(
                        xloc: 100.0 * Random.rand, yloc: 100.0 * Random.rand,
                        width: 300.0 * Random.rand, height: 200.0 * Random.rand)
      drawrectangle_element.fillcolor = MIColor.make_rgbacolor(
                                Random.rand, Random.rand, Random.rand)
      create_drawelement = CommandModule.make_drawelement(receiver_object,
                                drawinstructions: drawrectangle_element)
      smig_commands.add_command(create_drawelement)
    end
  end

  def self.create_drawarrayofelementscommand(receiver_object, 
                                                number_of_rectangles: 500)
    arrayofelements = MIDrawElement.new(:arrayofelements)
    number_of_rectangles.times do
      drawrectangle_element = MIDrawElement.new(:fillrectangle)
      drawrectangle_element.rectangle = MIShapes.make_rectangle(
                        xloc: 100.0 * Random.rand, yloc: 100.0 * Random.rand,
                        width: 300.0 * Random.rand, height: 200.0 * Random.rand)
      drawrectangle_element.fillcolor = MIColor.make_rgbacolor(
                                Random.rand, Random.rand, Random.rand)
      arrayofelements.add_drawelement_toarrayofelements(drawrectangle_element)
    end
    CommandModule.make_drawelement(receiver_object,
                                   drawinstructions: arrayofelements)
  end

  def self.create_drawarrayofroundedrectanglescommand(receiver_object, 
                                                number_of_rectangles: 500)
    arrayofelements = MIDrawElement.new(:arrayofelements)
    number_of_rectangles.times do
      drawrectangle_element = MIDrawElement.new(:fillpath)
      rounded_rect_path = MIPath.new
      the_rect = MIShapes.make_rectangle(xloc: 10 + 100 * Random.rand,
                                         yloc: 10 + 100 * Random.rand,
                                         width: 40 + 300 * Random.rand,
                                         height: 30 + 200 * Random.rand)
      radiuses = [ 1 + 20.0 * Random.rand, 1 + 20.0 * Random.rand,
                   1 + 20.0 * Random.rand, 1 + 20.0 * Random.rand ]
      rounded_rect_path.add_roundedrectangle_withradiuses(the_rect,
                                                          radiuses: radiuses)
      drawrectangle_element.arrayofpathelements = rounded_rect_path
      drawrectangle_element.startpoint = MIShapes.make_point(0, 0)
      drawrectangle_element.fillcolor = MIColor.make_rgbacolor(
                                Random.rand, Random.rand, Random.rand)
      arrayofelements.add_drawelement_toarrayofelements(drawrectangle_element)
    end
    CommandModule.make_drawelement(receiver_object,
                                   drawinstructions: arrayofelements)
  end

  def self.create_drawarrayofequationrectanglescommand(receiver_object, 
                                                number_of_rectangles: 500)
    arrayofelements = MIDrawElement.new(:arrayofelements)
    number_of_rectangles.times do
      drawrectangle_element = MIDrawElement.new(:fillpath)
      rounded_rect_path = MIPath.new
      the_rect = MIShapes.make_rectangle(xloc: "10 + 100 * $xloc_random",
                                         yloc: "10 + 100 * $yloc_random",
                                         width: "40 + 300 * $width_random",
                                         height: "30 + 200 * $height_random")
      radiuses = [ "1 + 20.0 * $radius1", "1 + 20.0 * $radius2",
                   "1 + 20.0 * $radius3", "1 + 20.0 * $radius4" ]
      rounded_rect_path.add_roundedrectangle_withradiuses(the_rect,
                                                          radiuses: radiuses)
      drawrectangle_element.arrayofpathelements = rounded_rect_path
      drawrectangle_element.startpoint = MIShapes.make_point(0, 0)
      drawrectangle_element.fillcolor = MIColor.make_rgbacolor(
                                Random.rand, Random.rand, Random.rand)
      variables_dict = { xloc_random: Random.rand, yloc_random: Random.rand,
                         width_random: Random.rand, height_random: Random.rand,
                         radius1: Random.rand, radius2: Random.rand,
                         radius3: Random.rand, radius4: Random.rand }
      drawrectangle_element.variables = variables_dict
      arrayofelements.add_drawelement_toarrayofelements(drawrectangle_element)
    end
    CommandModule.make_drawelement(receiver_object,
                                   drawinstructions: arrayofelements)
  end
end

num_rects = 40
smig_commands = CommandModule::SmigCommands.new
window_object = smig_commands.make_createwindowcontext()
MovingImagesPerformance.create_drawrectanglecommands(smig_commands,
                window_object, number_of_rectangles: num_rects)
puts "============================="
puts "#{num_rects} commands each drawing 1 rect to a window context"
time = Smig.perform_timed_commands(smig_commands)
rate = (num_rects/time).to_i
time = sprintf "%.4f", time
puts "Took #{time} to fill #{num_rects} rectangles at #{rate} per sec"

num_rects = 5000
smig_commands = CommandModule::SmigCommands.new
bitmap_object = smig_commands.make_createbitmapcontext()
MovingImagesPerformance.create_drawrectanglecommands(smig_commands,
                bitmap_object, number_of_rectangles: num_rects)
puts "============================="
puts "#{num_rects} commands each drawing 1 rect to an ARGB8bpc bitmap context"
time = Smig.perform_timed_commands(smig_commands)
rate = (num_rects/time).to_i
time = sprintf "%.4f", time
puts "Took #{time} to fill #{num_rects} rectangles at #{rate} per sec"

num_rects = 100
smig_commands = CommandModule::SmigCommands.new
bitmap_object = smig_commands.make_createbitmapcontext()
MovingImagesPerformance.create_drawrectanglecommands(smig_commands,
                bitmap_object, number_of_rectangles: num_rects)
start_time = Time.now
Smig.perform_debugcommands(smig_commands)
time = Time.now - start_time
rate = (num_rects/time).to_i
time = sprintf "%.4f", time
puts "============================="
puts "#{num_rects} commands sent individually drawing 1 rect to an ARGB8bpc bitmap context"
puts "Took #{time} to fill #{num_rects} rectangles at #{rate} per sec"

num_rects = 5000
smig_commands = CommandModule::SmigCommands.new
window_object = smig_commands.make_createwindowcontext(addtocleanup: false)
draw_command = MovingImagesPerformance.create_drawarrayofelementscommand(
                          window_object, number_of_rectangles: num_rects)
smig_commands.add_command(draw_command)
puts "============================="
puts "A single command drawing #{num_rects} rects to a window context"
time = Smig.perform_timed_commands(smig_commands)
rate = (num_rects/time).to_i
time = sprintf "%.4f", time
puts "Took #{time} to fill #{num_rects} rectangles at #{rate} per sec"
sleep(1)
Smig.close_object(window_object)

smig_commands = CommandModule::SmigCommands.new
bitmap_object = smig_commands.make_createbitmapcontext()
draw_command = MovingImagesPerformance.create_drawarrayofelementscommand(
                          bitmap_object, number_of_rectangles: num_rects)
smig_commands.add_command(draw_command)
puts "============================="
puts "A single command drawing #{num_rects} rects to a bitmap context"
time = Smig.perform_timed_commands(smig_commands)
rate = (num_rects/time).to_i
time = sprintf "%.4f", time
puts "Took #{time} to fill #{num_rects} rectangles at #{rate} per sec"

num_rects = 2000
smig_commands = CommandModule::SmigCommands.new
window_object = smig_commands.make_createwindowcontext(addtocleanup: false)
draw_cmd = MovingImagesPerformance.create_drawarrayofroundedrectanglescommand(
                          window_object, number_of_rectangles: num_rects)
smig_commands.add_command(draw_cmd)
puts "============================="
puts "A single command drawing #{num_rects} rounded rects to a window context"
time = Smig.perform_timed_commands(smig_commands)
rate = (num_rects/time).to_i
time = sprintf "%.4f", time
puts "Took #{time} to fill #{num_rects} rectangles at #{rate} per sec"
sleep(1)
Smig.close_object(window_object)

smig_commands = CommandModule::SmigCommands.new
bitmap_object = smig_commands.make_createbitmapcontext()
draw_cmd = MovingImagesPerformance.create_drawarrayofroundedrectanglescommand(
                          bitmap_object, number_of_rectangles: num_rects)
smig_commands.add_command(draw_cmd)
puts "============================="
puts "A single command drawing #{num_rects} rounded rects to an ARGB8bpc bitmap context"
time = Smig.perform_timed_commands(smig_commands)
rate = (num_rects/time).to_i
time = sprintf "%.4f", time
puts "Took #{time} to fill #{num_rects} rectangles at #{rate} per sec"

results_dir = File.expand_path('~/Desktop/smig_performance/')
commands_dir = File.expand_path('~/Desktop/input_performancecommands/')
FileUtils.mkdir_p(results_dir)
FileUtils.mkdir_p(commands_dir)

=begin
num_rects = 500
smig_commands = CommandModule::SmigCommands.new
window_object = smig_commands.make_createwindowcontext(addtocleanup: false)
draw_cmd = MovingImagesPerformance.create_drawarrayofequationrectanglescommand(
                          window_object, number_of_rectangles: num_rects)
smig_commands.add_command(draw_cmd)
puts "============================="
puts "Command drawing #{num_rects} rounded rects with equations to a window."
time = Smig.perform_timed_commands(smig_commands)
rate = (num_rects/time).to_i
time = sprintf "%.4f", time
puts "Took #{time} to fill #{num_rects} rectangles at #{rate} per sec"
sleep(1)
Smig.close_object(window_object)

smig_commands = CommandModule::SmigCommands.new
bitmap_object = smig_commands.make_createbitmapcontext()
draw_cmd = MovingImagesPerformance.create_drawarrayofequationrectanglescommand(
                          bitmap_object, number_of_rectangles: num_rects)
smig_commands.add_command(draw_cmd)
puts "============================="
puts "Command drawing #{num_rects} rounded rects with formula to bitmap context"
time = Smig.perform_timed_commands(smig_commands)
rate = (num_rects/time).to_i
time = sprintf "%.4f", time
puts "Took #{time} to fill #{num_rects} rectangles at #{rate} per sec"

num_queues = 4
num_rects_per_queue = 5000
results_dir = File.expand_path('~/Desktop/smig_performance/')
commands_dir = File.expand_path('~/Desktop/input_performancecommands/')
FileUtils.mkdir_p(results_dir)
FileUtils.mkdir_p(commands_dir)
commands_array = []
results_array = []
num_queues.times do |i|
  results_file = File.join(results_dir, "#{SecureRandom.uuid}.json")
  results_array.push(results_file)
  commands_file = File.join(commands_dir, "#{SecureRandom.uuid}.json")
  commands_array.push(commands_file)
  smig_commands = CommandModule::SmigCommands.new
  bitmap_object = smig_commands.make_createbitmapcontext()
  draw = MovingImagesPerformance.create_drawarrayofequationrectanglescommand(
                      bitmap_object, number_of_rectangles: num_rects_per_queue)
  smig_commands.add_command(draw)
  smig_commands.run_asynchronously = ((i == (num_queues - 1)) ? false : true)
  smig_commands.informationreturned = :lastcommandresult
  smig_commands.saveresultstype = :jsonfile
  smig_commands.saveresultsto = results_file
  open(commands_file, 'w') { |f| f.puts smig_commands.commandshash.to_json }
end

startTime = Time.now
commands_array.each do |command_file|
  Smig.perform_jsoncommands(jsonfile: command_file)
  FileUtils.rm_f(command_file)
end

total_drawn_rects = num_queues * num_rects_per_queue
time_to_draw = Time.now - startTime
rate_drawn = (total_drawn_rects/time_to_draw).to_i
time_to_draw = sprintf "%.3f", time_to_draw
puts "======================================"
puts "Running concurrent #{num_queues} processing queues"
puts "Number of equationed rounded rectangles drawn #{total_drawn_rects}"
puts "Time to process: #{time_to_draw} in seconds"
puts "Equationed rounded rectangles drawn at: #{rate_drawn}/sec"

results_array.each { |file_path| FileUtils.rm_f(file_path) }
=end

num_queues = 6
num_rects_per_queue = 20000
commands_array = []
results_array = []
num_queues.times do |i|
  results_file = File.join(results_dir, "#{SecureRandom.uuid}.json")
  results_array.push(results_file)
  commands_file = File.join(commands_dir, "#{SecureRandom.uuid}.json")
  commands_array.push(commands_file)
  smig_commands = CommandModule::SmigCommands.new
  bitmap_object = smig_commands.make_createbitmapcontext()
  draw = MovingImagesPerformance.create_drawarrayofelementscommand(
                      bitmap_object, number_of_rectangles: num_rects_per_queue)
  smig_commands.add_command(draw)
  smig_commands.run_asynchronously = ((i == (num_queues - 1)) ? false : true)
  smig_commands.informationreturned = :lastcommandresult
  smig_commands.saveresultstype = :jsonfile
  smig_commands.saveresultsto = results_file
  open(commands_file, 'w') { |f| f.puts smig_commands.commandshash.to_json }
end

startTime = Time.now
commands_array.each do |command_file|
  Smig.perform_jsoncommands(jsonfile: command_file)
  FileUtils.rm_f(command_file)
end

total_drawn_rects = num_queues * num_rects_per_queue
time_to_draw = Time.now - startTime
rate_drawn = (total_drawn_rects/time_to_draw).to_i
time_to_draw = sprintf "%.3f", time_to_draw
puts "======================================"
puts "Running concurrent #{num_queues} processing queues"
puts "Number of rectangles drawn #{total_drawn_rects}"
puts "Time to process: #{time_to_draw} in seconds"
puts "Fill simple rectangles drawn at: #{rate_drawn}/sec"

results_array.each { |file_path| FileUtils.rm_f(file_path) }

puts "=========================================="
puts "finished"
