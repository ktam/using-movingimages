#!/bin/sh

# Setup some defaults.
SMIG="smig" # If needed replace smig here with a full path

# Display a choose file dialog to select image file. If user didn't select ok 
# then exit the script.
SOURCEFILE=`osascript -e "POSIX path of (choose file with prompt \"Select an image file\" default location (path to pictures folder))"` # ask for a file. Returns 127 if user cancels.
if [ $? -ne 0 ]; then
    echo "1. User cancelled selecting an image file."
    exit 1
fi

### Now create the image importer using the selected image file.
### If a image importer isn't created then we exit.
IIR=`smig create -type imageimporter -file "$SOURCEFILE"`
if [ $? -ne 0 ]; then
    echo "2. Failed to create an image importer with selected file: $SOURCEFILE"
    exit 1
fi

# importer object created.
echo "Image importer object reference is: $IIR"

# We wont to make sure the longest dimension is scaled to 512 pixels.
MAXSIZE=512

### get the width and height of the image.
PIXELWIDTH=`smig getproperty -object $IIR -imageindex 0 -property dictionary.PixelWidth`
PIXELHEIGHT=`smig getproperty -object $IIR -imageindex 0 -property dictionary.PixelHeight`

echo Width of image is: $PIXELWIDTH
echo Height of image is: $PIXELHEIGHT

# Calculate the scaling factor
# The scale=8 specifies the number of decimal places to calculate the number to.
if [[ $PIXELWIDTH -gt $PIXELHEIGHT ]]; then
  echo "3. Width of image is larger than height"
  SCALE=`bc <<< "scale=8;$MAXSIZE/$PIXELWIDTH"`
else
	echo "4. Height of image is larger than width"
  SCALE=`bc <<< "scale=8;$MAXSIZE/$PIXELHEIGHT"`
fi

# Calculate the height and width of the bitmap context to create.
# When creating the bitmap context we need the dimensions specified as integer
# values setting scale=0 sets the number of decimal places for height and width
# to zero.
# bc rounds down to the nearest whole value. We want to round to nearest value.
# Since the numbers are positive adding 0.5 will do the correct rounding.
NEWHEIGHT=`bc <<< "scale=0;(($SCALE*$PIXELHEIGHT)+0.5)/1"`
NEWWIDTH=`bc <<< "scale=0;(($SCALE*$PIXELWIDTH)+0.5)/1"`

echo "New width is: $NEWWIDTH"
echo "New height is: $NEWHEIGHT"

### Calculated the desired height and width, now create the bitmap context.
BMCR=`$SMIG create -type nsgraphicscontext -width $NEWWIDTH -height $NEWHEIGHT -xloc 200 -yloc 100`

echo "Bit map reference is: $BMCR"

### Draw the image from the image file into the bitmap context.
$SMIG doaction -drawelement -object $BMCR -jsonstring '{ "elementtype" : "drawimage", "sourceobject" : { "objectreference" : '$IIR' }, "destinationrectangle" : { "origin" : { "x" : 0.0, "y" : 0.0 }, "size" : { "width" : '$NEWWIDTH', "height" : '$NEWHEIGHT' } } }'

echo "Display the image for 10 seconds and then exit"

# Pause the script for 10 seconds whilst the image is displayed.
sleep 10

### We don't need the image importer anymore. Close it.
$SMIG doaction -close -object $IIR

# Wait for 10 seconds whilst the image is displayed.
sleep 10

### Close the bitmap context object.
$SMIG doaction -close -object $BMCR
