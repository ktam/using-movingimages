MovingImages Readme.

This is the second alpha release of MovingImages and is version (0.2a).

There is plenty of useful functionality now available for manipulating image files but the really interesting functionality is not yet implemented.

This version adds:

• Added the ability to setup and render core image filter chains.
• Added logging functionality useful for debugging drawing and filter rendering commands
• Added a chroma key filter to the list of available core image filters.
• Added an option to return a list of core graphics blend modes
• Added option for naming base objects when they are created for later identification.
• Added system tests for MovingImages
• Added a get pixel data option
• Added affine transform as an option for the various draw element commands.
• Added affine transform as an optional property for drawing text
• Added text alignment as a drawbasicstring attribute
• Added a coreimageblend script to be distributed with MovingImages
• Added a do transition script to be distributed with MovingImages
• Added a simplesinglecifilter script to be distributed with MovingImages

• Updated documentation
	• Please see: https://gitlab.com/ktam/using-movingimages/wikis/Contents

For the license agreement please read the LICENSE file.

=======================================
Installation Instructions

Installation is made up of two steps.

1. After downloading the MovingImages disk image and mounting the disk image, drag the MovingImages application to the Applications folder.

2. Run the MovingImages application.
	• Click the “Install smig” button.
		The version number should appear below.
	• Click the “Load” button in the MovingImages Launch Agent section.
	• Click the “Get Version” button.
		The version number should be displayed after “Version:”.
		You should be informed that the Launch Agent is running.
		After 12 seconds you should be informed the LaunchAgent is not running.
	• Click “Install Location” to select a folder for the image processing scripts.
		Select a folder and click Choose. Whatever folder you select I would
		recommend adding that folder to your unix path otherwise you will
		have to include the full path to the script.
	• Click the “Install scripts” button.

You are now ready to go.

