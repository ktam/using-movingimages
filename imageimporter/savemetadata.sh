#!/bin/sh
SMIG="smig" #If necessary replace smig here with a full path, or in future with debug smig dsmig
OUTPUTDIR="$HOME/Desktop/junk/" # output from scripts goes here.
mkdir -p $OUTPUTDIR # create output directory if it doesn't already exist

# Ask for a file. Returns 127 if user cancels.
SOURCEFILE=`osascript -e "POSIX path of (choose file with prompt \"Select an image file\" default location (path to pictures folder))"`

if [[ $? -ne 0 ]]; then
    exit 1
fi

#######
IMAGEIMPORTERREFERENCE=`"$SMIG" create -type imageimporter -file "$SOURCEFILE"`
#######

FILENAME=`basename "$SOURCEFILE"`
FILENAME=${FILENAME%.*}

#######
$SMIG getproperties -object $IMAGEIMPORTERREFERENCE -imageindex 0 -propertyfile "$OUTPUTDIR$FILENAME.json" # save the metadata as a json file to the output directory.
$SMIG doaction -close -object $IMAGEIMPORTERREFERENCE # close the image importer
#######

# open the generated json file with your default editor.
open "$OUTPUTDIR$FILENAME.json"
