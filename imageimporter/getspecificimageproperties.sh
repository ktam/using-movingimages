#!/bin/sh
SMIG="smig" # replace smig here with a full path, or in future with debug dsmig

# Ask for a file. Returns 127 if user cancels.
SOURCEFILE=`osascript -e "POSIX path of (choose file with prompt \"Select an image file\" default location (path to pictures folder))"`
if [[ $? -ne 0 ]]; then
    exit 1
fi

#######
IMAGEIMPORTERREFERENCE=`"$SMIG" create -type imageimporter -file "$SOURCEFILE"`
#######

FILENAME=`basename "$SOURCEFILE"`
FILENAME=${FILENAME%.*}

#######
PIXELWIDTH=`$SMIG getproperty -object $IMAGEIMPORTERREFERENCE -imageindex 0 -property dictionary.PixelWidth`
EXPOSURETIME=`$SMIG getproperty -object $IMAGEIMPORTERREFERENCE -imageindex 0 -property dictionary.\{Exif\}.ExposureTime`
$SMIG doaction -close -object $IMAGEIMPORTERREFERENCE # close the image importer
#######

echo "Width of image (pixels): " $PIXELWIDTH
echo "Image exposure time (secs): " $EXPOSURETIME
