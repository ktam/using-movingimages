set numColors to 5 -- number of posterize colors per color component (red/green/blue).

-- Set the path to the property list file which we will save with the posterize filter chain definition.
set tempFilePath to ((path to desktop) as text) & "deleteme.plist"
set posixTempFilePath to quoted form of (POSIX path of tempFilePath)

-- This function creates the AppleScript record which will be converted later into the property list file.
on CreatePosterFilterPropertyList(numColors, inputImageRef, imageIndex, renderDestRef)
	set renderDestDict to {objectreference:renderDestRef as integer}
	set numColorsDict to {cifilterkey:"inputLevels", cifiltervalue:numColors}
	set inputImageDict to {cifilterkey:"inputImage", cifiltervalue:{objectreference:inputImageRef as integer}, cifiltervalueclass:"CIImage"}
	set posterizeFilterDict to {cifiltername:"CIColorPosterize", cifilterproperties:[numColorsDict, inputImageDict]}
	set filterChainDict to {cirenderdestination:renderDestDict, cifilterlist:[posterizeFilterDict]}
	return filterChainDict
end CreatePosterFilterPropertyList

-- The do shell script shell does not include /usr/local/bin in its path so hard code path to smig
set smigPath to quoted form of "/usr/local/bin/smig"

-- Choose the image file to posterize
set imageFile to POSIX path of (choose file with prompt "Select an image file to posterize" default location (path to pictures folder))

-- Select the destination location for the posterized image file.
set destinationFile to POSIX path of (choose file name with prompt "Save the new posterize image file: " default name "Posterized.jpg" default location (path to desktop))

-- Create the image importer object
set importerRef to do shell script smigPath & " create -type imageimporter -name com.yvs.example.applescript.importer -file " & quoted form of imageFile
importerRef

-- Get the width and height of the image in the image file.
set imageWidthStr to do shell script smigPath & " getproperty -object " & importerRef & " -imageindex 0 -property dictionary.PixelWidth"
set imageHeightStr to do shell script smigPath & " getproperty -object " & importerRef & " -imageindex 0 -property dictionary.PixelHeight"

-- Create the bitmap context where the posterized image will be rendered to.
set bitmapContextRef to do shell script smigPath & " create -type bitmapcontext -preset AlphaPreMulFirstRGB8bpcInt -width " & imageWidthStr & " -height " & imageHeightStr

-- Create the dictionary that represents the posterize filter chain.
set myDict to my CreatePosterFilterPropertyList(numColors, importerRef, 0, bitmapContextRef)

-- Save the dictionary as a property list file to the temporary file location.
tell application "System Events"
	set propertyListItem to make new property list item with properties {kind:record, value:myDict}
	make new property list file with properties {contents:propertyListItem, name:tempFilePath}
end tell

-- Create the image filter chain, by reading in the property list file just created.
set filterChainRef to do shell script smigPath & " create -type imagefilterchain -propertyfile " & posixTempFilePath

-- Render the filter chain to the render destination which is the bitmap context.
do shell script smigPath & " doaction -renderfilterchain -object " & filterChainRef

-- Create the image exporter object
set imageExporter to do shell script smigPath & " create -type imageexporter -utifiletype public.jpeg -file " & destinationFile

-- Add an image to the image exporter object which we have generated from the bitmap context just rendered to.
do shell script smigPath & " doaction -addimage -object " & imageExporter & " -secondaryobject " & bitmapContextRef

-- Export the image using the image exporter object
do shell script smigPath & " doaction -export -object " & imageExporter

-- Close all the Moving Images we have created. This is a bit of a nasty shortcut. If you have multiple scripts driving 
-- Moving Images then this action will also close objects they have created. Alternately you can close one at a time. etc.
-- "/usr/local/bin/smig doaction -close -object " & imageExporter
do shell script smigPath & " doaction -closeall"

-- Delete the temporary file which contains the property list representation of the filter chain.
do shell script "rm " & posixTempFilePath
