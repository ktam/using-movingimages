dotransition --count 25 --sourceimage "~/Desktop/exampleimages/DSCN0744.JPG" --targetimage "~/Desktop/exampleimages/DSCN0746.JPG" --transitionfilter CIBarsSwipeTransition --width 20 --angle=1.57 --baroffset 10 --outputdir "~/Desktop/junkimages/barsswipetransition" --basename BarsSwipeTransition --exportfiletype public.jpeg --verbose

dotransition --count 15 --sourceimage "~/Desktop/exampleimages/DSCN0744.JPG" --targetimage "~/Desktop/exampleimages/DSCN0746.JPG" --transitionfilter CIRippleTransition --width 20 --scale 50 --extent 0,0,908,681 --outputdir "~/Desktop/junkimages/rippletransition" --center 454,340 --basename Ripple --exportfiletype public.jpeg

dotransition --count 15 --sourceimage "~/Desktop/exampleimages/DSCN0744.JPG" --targetimage "~/Desktop/exampleimages/DSCN0746.JPG" --transitionfilter CISwipeTransition --angle 0.77 --width 50 --extent 0,0,908,681 --outputdir "~/Desktop/junkimages/swipetransition" --opacity 0.5 --color 1,0,1,1 --basename Swipe --exportfiletype public.tiff

dotransition --count 20 --sourceimage "~/Desktop/exampleimages/DSCN0744.JPG" --targetimage "~/Desktop/exampleimages/DSCN0746.JPG" --transitionfilter CICopyMachineTransition --angle 0 --width 80 --extent 0,0,908,681 --outputdir "~/Desktop/junkimages/copymachinetransition" --opacity 0.8 --color 0,1,0,1 --basename CopyMachine --exportfiletype public.jpeg

dotransition --count 20 --sourceimage "~/Desktop/exampleimages/DSCN0744.JPG" --targetimage "~/Desktop/exampleimages/DSCN0746.JPG" --transitionfilter CIDisintegrateWithMaskTransition --shadowoffset=-8,12 --shadowdensity 0.5 --shadowradius 50 --maskimage "~/Desktop/exampleimages/MovingImagesMaskInvert.png" --outputdir "~/Desktop/junkimages/disintegrate/" --basename WithMask --exportfiletype public.jpeg

dotransition --count 30 --sourceimage "~/Desktop/exampleimages/DSCN0744.JPG" --targetimage "~/Desktop/exampleimages/DSCN0746.JPG" --transitionfilter CIFlashTransition --striationstrength 0.8 --color 1.0,0.4,0.1,1.0 --fadethreshold 0.7 --extent 0,0,908,681 --center 454,340 --striationcontrast 1.375 --outputdir "~/Desktop/junkimages/flashtransition" --basename Flash --exportfiletype public.jpeg --verbose

dotransition --count 20 --sourceimage "~/Desktop/exampleimages/DSCN0744.JPG" --targetimage "~/Desktop/exampleimages/DSCN0746.JPG" --transitionfilter CIDissolveTransition --outputdir "~/Desktop/junkimages/dissolvetransition" --basename Dissolve --exportfiletype public.jpeg

# The following uses the default value for compression. It is not specified.
dotransition --count 40 --sourceimage "~/Desktop/exampleimages/DSCN0744.JPG" --targetimage "~/Desktop/exampleimages/DSCN0746.JPG" --transitionfilter CIModTransition --angle 2.0 --center 454,340 --radius 130 --outputdir "~/Desktop/junkimages/modtransition" --basename Mod --exportfiletype public.jpeg

dotransition --count 20 --sourceimage "~/Desktop/exampleimages/DSCN0744.JPG" --targetimage "~/Desktop/exampleimages/DSCN0733.JPG" --backsideimage "~/Desktop/exampleimages/DSCN0746.JPG" --transitionfilter CIPageCurlTransition --angle=-2.7 --extent 0,0,908,681 --radius 120 --outputdir "~/Desktop/junkimages/pagecurltransition" --basename PageCurl --exportfiletype public.jpeg --verbose

dotransition --count 20 --sourceimage "~/Desktop/exampleimages/DSCN0744.JPG" --targetimage "~/Desktop/exampleimages/DSCN0733.JPG" --backsideimage "~/Desktop/exampleimages/DSCN0746.JPG" --transitionfilter CIPageCurlWithShadowTransition --angle=-2.7 --extent 0,0,908,681 --radius 120 --outputdir "~/Desktop/junkimages/pagecurlshadowtransition" --basename PageCurlShadow --exportfiletype public.jpeg --shadowextent 0,0,200,200 --shadowamount 0.7 --shadowsize 0.5
